FROM jacekmarchwicki/android:java7
MAINTAINER John Hamelink <john@farmer.io>

# Install sdk elements
ADD android-update.exp /tmp/

RUN set -x \
   && expect -f /tmp/android-update.exp android-19,tool,platform-tool,build-tools-19,extra-android-support,platform-tools,tools,build-tools-19,addon-google_apis_x86-google-19,extra-android-support,extra-android-m2repository,extra-google-m2repository,sys-img-armeabi-v7a-android-19,sys-img-armeabi-x86-android-19

RUN curl https://github.com/ryotarai/go-gist/releases/download/v0.1.0/go-gist-linux-amd64 > gist \
   && chmod +x gist

RUN which adb
RUN which android

